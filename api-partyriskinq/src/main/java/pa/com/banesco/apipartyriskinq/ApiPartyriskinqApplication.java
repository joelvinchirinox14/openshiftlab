package pa.com.banesco.apipartyriskinq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ApiPartyriskinqApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiPartyriskinqApplication.class, args);
	}
    @GetMapping("/api/v1/saludo/{nombre}")
	public String saludo(@PathVariable String nombre) {

		return "Hola como estas  " + nombre;
	}
    
    
//    @GetMapping("/api/v1/partyris/{nombre}")
//	public String saludo(@PathVariable String nombre) {
//
//		return "Hola como estas  " + nombre;
//	}


}
